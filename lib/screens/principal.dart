import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:http/http.dart' as http;

import '../widgets/custom_text_form_field.dart';

class HousePricesInterface extends StatefulWidget {
  const HousePricesInterface({Key? key}) : super(key: key);

  @override
  State<HousePricesInterface> createState() => _HousePricesInterfaceState();
}

class _HousePricesInterfaceState extends State<HousePricesInterface> {
  Map<String, double> sendValues = {
    "BsmtFinSF1": 0,
    'GarageYrBlt': 0,
    "FirstFlrSF": 0,
    "GarageArea": 0,
    "TotalBsmtSF": 0,
    "YearBuilt": 0,
    "GarageCars": 0,
    "GrLivArea": 0,
    "OverallQual": 0
  };

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 63, 181, 157),
          title: const Text('House Prices ML')),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              const SizedBox(height: 30),
              CustomTextFormField(
                  labelText: 'Basement finished area',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'BsmtFinSF1',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Year garage was built',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'GarageYrBlt',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'First Floor square feet',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'FirstFlrSF',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Size of garage in square feet',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'GarageArea',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Total square feet of basement area',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'TotalBsmtSF',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Original construction date',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'YearBuilt',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Size of garage in car capacity',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'GarageCars',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText: 'Above grade (ground) living area square feet',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'GrLivArea',
                  formValues: sendValues),
              CustomTextFormField(
                  labelText:
                      'Rates the overall material and finish of the house',
                  hintText: '0',
                  keyboardType: TextInputType.number,
                  formProperty: 'OverallQual',
                  formValues: sendValues),
              const SizedBox(height: 60),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.indigo, shape: const StadiumBorder()),
                  onPressed: () async {
                    if (!formKey.currentState!.validate()) {
                      //FocusScope.of(context).requestFocus(FocusNode());
                      return;
                    }
                    print(sendValues);
                    final response = await http.post(
                        Uri.parse(
                            'https://fosvargas-house-prices.herokuapp.com/prediccion'),
                        headers: <String, String>{
                          'Content-Type': 'application/json; charset=UTF-8',
                        },
                        body: json.encode(sendValues));
                    print('Code Status: ${response.statusCode}');
                    print('Response Body: ${response.body}');
                    dynamic prediccion;
                    if (response.statusCode == 201) {
                      final jsonResponse = jsonDecode(response.body);
                      prediccion = (jsonResponse['predicted_price']);
                    }

                    Alert(
                      context: context,
                      title: "",
                      desc: "The price of the house will be: $prediccion",
                      buttons: [
                        DialogButton(
                          onPressed: () => Navigator.pop(context),
                          width: 120,
                          child: const Text(
                            "OK",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        )
                      ],
                    ).show();
                  },
                  child: const SizedBox(
                      width: double.infinity,
                      child: Center(child: Text('Predict'))))
            ],
          ),
        ),
      )),
    );
  }
}
